#ifndef DRAWDRONE_H
#define DRAWDRONE_H

#include <GL/glut.h>
#include <stdio.h>

#include "debug.h"
#include "basicfig.h"



/* Functions */
void landingsPoot(const int lengte, const float verp, const int hoekLand, GLenum mode);
void propellor(const int hoekProp, GLenum mode, GLint q, int showPoints);
void bezier(GLenum mode, int showPoints);
void bSpline(GLenum mode, int showPoints);
void lichaam(GLenum mode, int showPoints);
void frame(GLenum mode, GLint q);


#endif
