#ifndef MATERIALS_H
#define MATERIALS_H

#include <stdio.h>
#include <malloc.h>
#include <stdlib.h>
#include "debug.h"

#define     NLEN    15
#define     VLEN    4
#define     DATA    "materiaal.txt"

/* Materials */
struct Materiaal{
    char name[NLEN];
    float ambient[VLEN];
    float diffuus[VLEN];
    float specular[VLEN];
    float shininess;
};

/* Functions */
struct Materiaal * initMaterials(const int visual, int * aantal);

#endif
