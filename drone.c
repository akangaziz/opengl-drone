#include "drone.h"
#include "InitJPG.h"

/* Global vars */
/* Colours  lights */
const GLfloat zwart[] = {0, 0, 0, 1};
const GLfloat wit[] = {1, 1, 1, 1};
const GLfloat lv2[] = {0, 1, 1, 1};
const GLfloat lv3[] = {1, 0.2, 0.2, 1};
const GLfloat lv4[] = {0.8, 0.8, 0.8, 1};

/* Positions lights */
GLfloat lp1[] = {0, 0, 0, 0};
GLfloat lp2[] = {100, 80, 0, 1};
GLfloat lp3[] = {-100, 80, 0, 1};
GLfloat lp4[] = {50, 5, 50, 1};
GLfloat spotRichting[] = {0, -1, 0};
GLfloat spotHoek = 30;
GLfloat spotExp = 4;

/* fog */
GLfloat density = 0.3;
GLfloat fogColor[4] = {0, 0, 0, 1};

/* Materials */
struct Materiaal *materials;
int aantMat = 0;
int matFrame = 21;
int matProp = 19;
int matLich = 24;
int matPoot = 13;
int matGround = 0;
int extraShininess = 0;

/* Textures */
GLuint texName[AANTTEXT];
int textuur = 0;

/* enable vars */
int fog = 0;
int blend = 0;
int draai = 0;
int vlieg = 0;
int q = 32;
GLenum mode = GL_FILL;
int showPoints = 0;

/* Position camera */
GLint eyesx = 200;
GLint eyesy = 200;
GLint eyesz = 200;
/* position refpoint */
GLint visx = 0;
GLint visy = 0;
GLint visz = 0;
/* Ortho, fustrum or perspective ? */
GLint vis = 2;

/* animate var */
int hoekDrone = 0;
int hoekProp = 0;
int hoekLand = 0;
unsigned int hoogte = 0;
unsigned int maxHoogte = 100;
int tijd = 20;

void init()
{
    /* Init materials */
    materials = initMaterials(1, &aantMat);

    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
    glutInitWindowSize(800,800);
    glutInitWindowPosition(100,100);

    glutCreateWindow(TITLEPROG);
    
    glutDisplayFunc(display);
    glutReshapeFunc(raam);
    glutKeyboardFunc(toetsen);

    glClearColor(0.5, 0.5, .5, 1);
    glClearDepth(1);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_NORMALIZE);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    /* Lights */
	initLicht();

    /* Init textures */
    initTexture();

    /* init fog */
    glFogfv (GL_FOG_COLOR, fogColor);
    glFogi (GL_FOG_MODE, GL_EXP);
    glFogf (GL_FOG_DENSITY, density);
   
    /* Init menu */
    initMenu();
}

void initLicht()
{ 
    /*
     * Init function for the lights 
     * Light0 => white ambient 
     * Light1 => green/blue diffuse 
     * Light2 => red specular
     * Light3 => light gray diffuse spot
     */
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, zwart);

    glLightfv(GL_LIGHT1, GL_AMBIENT, wit);
    glLightfv(GL_LIGHT1, GL_DIFFUSE, zwart);
    glLightfv(GL_LIGHT1, GL_SPECULAR, zwart);
    
    glLightfv(GL_LIGHT2, GL_AMBIENT, zwart);
    glLightfv(GL_LIGHT2, GL_DIFFUSE, lv2);
    glLightfv(GL_LIGHT2, GL_SPECULAR, zwart);

    glLightfv(GL_LIGHT3, GL_AMBIENT, zwart);
    glLightfv(GL_LIGHT3, GL_DIFFUSE, zwart);
    glLightfv(GL_LIGHT3, GL_SPECULAR, lv3);

    glLightfv(GL_LIGHT4, GL_AMBIENT, zwart);
    glLightfv(GL_LIGHT4, GL_DIFFUSE, lv4);
    glLightfv(GL_LIGHT4, GL_SPECULAR, zwart);
    glLightf(GL_LIGHT4, GL_SPOT_CUTOFF, spotHoek);
    glLightf(GL_LIGHT4, GL_SPOT_EXPONENT, spotExp);
}

void initTexture()
{
    /*
     * Function for loading jpg's to be used as textures
     */

    tImageJPG *pImage;
    int i;

    glPixelStorei( GL_UNPACK_ALIGNMENT, 1);
    glGenTextures(AANTTEXT, texName);
    for(i=0;i< AANTTEXT;i++)
    {
        pImage = LoadJPG(beeldnaam[i]);
        glBindTexture (GL_TEXTURE_2D,texName[i]);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, pImage->sizeX, pImage->sizeY,
                0, GL_RGB, GL_UNSIGNED_BYTE, pImage->data);
    }
}

void initMenu()
{
    GLint submenu[AANTALMENU];
     
    int i, j;

    for(j=0; j<AANTALMENU; j++){
        submenu[j] = glutCreateMenu(myMenu);
        for(i=0; i<aantMat; i++){
            glutAddMenuEntry(materials[i].name, (j*100)+i);
        }
    }

    glutCreateMenu(myMenu);
    for(j=0; j<AANTALMENU; j++){
        glutAddSubMenu(menuNamen[j], submenu[j]);
    }

    glutAttachMenu(GLUT_RIGHT_BUTTON);
}

void toetsen(unsigned char key, int x, int y)
{ 
    /*
     * Keyboard interaction
     */
  
	switch(key){
		case 'g':
			if(vlieg == 0){
				draai ^= 1;
                glutTimerFunc(tijd,anim,1);
			}
			else{
				fprintf(stdout, "We zijn aan het vliegen. Kan propellors niet stoppen.\n");
			}
			break;
		case 'G':
			if((draai == 1) && ((vlieg == 0) || (vlieg == 2))){
				/* Opstijgen */
				vlieg = 1;
			}
			else if(draai == 1){
				/* Landen */
				vlieg = 2;
			}
			break;
		case 'h':
			if(maxHoogte == 0){
				fprintf(stdout, "We zitten op de grond.\n");
				vlieg = 0;
				maxHoogte = 100;
			}
			else {
				maxHoogte -= 10;
			}
			break;
		case 'H':
			maxHoogte += 10;
			break;
		case 's':
            glShadeModel(GL_FLAT);
			break;
		case 'S':
			glShadeModel(GL_SMOOTH);
			break;
        case 'x':
            eyesx += 10;
            break;
        case 'X':
            eyesx -= 10;
            break;
        case 'y':
            eyesy += 10;
            break;
        case 'Y':
            eyesy -= 10;
            break;
        case 'z':
            eyesz += 10;
            break;
        case 'Z':
            eyesz -= 10;
            break;
        case 'n':
            visx += 10;
            break;
        case 'N':
            visx -= 10;
            break;
        case 'o':
            visy += 10;
            break;
        case 'O':
            visy -= 10;
            break;
        case 'p':
            visz += 10;
            break;
        case 'P':
            visz -= 10;
            break;
        case 'a':
            glEnable(GL_LIGHT1);
            break;
        case 'A':
            glDisable(GL_LIGHT1);
            break;
        case 'b':
            glEnable(GL_LIGHT2);
            break;
        case 'B':
            glDisable(GL_LIGHT2);
            break;
        case 'c':
            glEnable(GL_LIGHT3);
            break;
        case 'C':
            glDisable(GL_LIGHT3);
            break;
        case 'd':
            glEnable(GL_LIGHT4);
            break;
        case 'D':
            glDisable(GL_LIGHT4);
            break;
        case 'f':
            blend = ~blend;
            break;
        case 't':
            textuur = ~textuur;
            break;
        case 'm':
            fog = ~fog;
            break;
        case 'l':
            if(mode == GL_FILL){
                mode = GL_LINE;
            }
            else{
                mode = GL_FILL;
            }
            break;
        case 'v':
            if(spotHoek < 90){
                spotHoek += 5;
            }
            glLightf(GL_LIGHT4, GL_SPOT_CUTOFF, spotHoek);
            break;
        case 'V':
            if(spotHoek > 0){
                spotHoek -= 5;
            }
            glLightf(GL_LIGHT4, GL_SPOT_CUTOFF, spotHoek);
            break;
        case 'w':
            spotExp += 5;
            glLightf(GL_LIGHT4, GL_SPOT_EXPONENT, spotExp);
            break;
        case 'W':
            spotExp -= 5;
            glLightf(GL_LIGHT4, GL_SPOT_EXPONENT, spotExp);
            break;
        case 'j':
            showPoints = ~ showPoints;
            break;
        case 'e':
            extraShininess += 5;
            break;
        case 'E':
            extraShininess -= 5;
            break;
        case 'q':
            free(materials);
            exit(0);
            break;
	}

    glutPostRedisplay();
}

void myMenu(int id)
{
    if(id < 100){
        /* In frame menu */
       matFrame = id; 
    }
    else if(id < 200){
        /* In Lichaam */
        id -= 100;
        matLich = id;
    }
    else if(id < 300){
        /* In proppellors */
        id -= 200;
        matProp = id;
    }
    else if(id < 400){
        /* In landing gear */
        id -= 300;
        matPoot = id;
    }
    else{
        /* Ground */
        id -= 400;
        matGround = id;
    }

    bugprint("%d %s\n", id, materials[id].name);

    glutPostRedisplay();
}

void raam(GLint newWidth, GLint newHeight)
{
    /* 
     * Callbackfunction when the display gets resized
     */

    glViewport(0,0,newWidth, newHeight);
    
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    if(vis == 0){
        glOrtho(-100, 100, -100, 100, 10, 1000);
    }
    else if(vis == 1){
        glFrustum(-100, 100, -100, 100, 100, 1000);
    }
    else{
        gluPerspective(60, 1, 1, 1000);
    }
}

void display()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    gluLookAt(eyesx,eyesy,eyesz, visx,visy,visz, 0,1,0);

    //Tekenen assenkruis
    glLineWidth(1);

    //y-as
    glColor3ub(0, 255, 0);
    glBegin(GL_LINE_STRIP);
        glVertex3f(0, 0, 0);
        glVertex3f(0, 100, 0);
    glEnd();

    //x-as
    glColor3ub(0, 0, 255);
    glBegin(GL_LINE_STRIP);
        glVertex3f(0, 0, 0);
        glVertex3f(100, 0, 0);
    glEnd();

    //z-as
    glColor3ub(255, 0, 0);
    glBegin(GL_LINE_STRIP);
        glVertex3f(0, 0, 0);
        glVertex3f(0, 0, 100);
    glEnd();


    /* Position lights */
    glLightfv(GL_LIGHT1, GL_POSITION, lp1);
    glLightfv(GL_LIGHT2, GL_POSITION, lp2);
    glLightfv(GL_LIGHT3, GL_POSITION, lp3);
    glLightfv(GL_LIGHT4, GL_SPOT_DIRECTION, spotRichting);

    /* Enable the lights */
    glEnable(GL_LIGHTING);

    /* Fog */
    if(fog){
        glEnable(GL_FOG);
    }

    /* Draw the ground */
    glPushMatrix();
        glTranslatef(-100, 0, -100);
        drawGround(TGRASS, 100, q);
    glPopMatrix();
    glPushMatrix();
        glTranslatef(0, 0, -100);
        drawGround(TGRASS, 100, q);
    glPopMatrix();
    glPushMatrix();
        glTranslatef(-100, 0, 0);
        drawGround(TGRASS, 100, q);
    glPopMatrix();
    glPushMatrix();
        drawGround(THELI, 100, q);
    glPopMatrix();

    /* Draw the drone */
    glPushMatrix();
		glRotatef(hoekDrone, 0, 1, 0);
		glTranslatef(0, hoogte, 0);
        
    glLightfv(GL_LIGHT4, GL_POSITION, lp4);
        drawDrone();
    glPopMatrix();

    /* Fog */
    if(fog){
        glDisable(GL_FOG);
    }

    glDisable(GL_LIGHTING);
    glutSwapBuffers();
}

void drawGround(int texture, GLdouble size, GLint q)
{
    /*
     * Function that draws the ground.
     * Give the number of the to be used texture
     */

    int i, j;
    GLdouble a = size / q;
    glPushMatrix();

    glNormal3f(0, 1, 0);
    /* Ground texture */
    if(textuur){
        glEnable(GL_TEXTURE_2D);
        glBindTexture (GL_TEXTURE_2D,texName[texture]);
        glTexEnvi(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_MODULATE);
        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
       
        for(i=0; i<=q; i++){
            for(j=0; j<=q; j++){
            glBegin(GL_QUADS);
                glTexCoord2f((i/(double)q), (j/(double)q));
                glVertex3f(i*a,0,j*a);
                glTexCoord2f(((i+1)/(double)q), (j/(double)q));
                glVertex3f(i*a+a,0,j*a);
                glTexCoord2f(((i+1)/(double)q), ((j+1)/(double)q)); 
                glVertex3f(i*a+a,0,j*a+a);
                glTexCoord2f((i/(double)q), ((j+1)/(double)q));
                glVertex3f(i*a,0,j*a+a);
           glEnd();
           }
        }
        glDisable(GL_TEXTURE_2D);
    }
    else{
        /* No ground texture */
        glMaterialfv(GL_FRONT, GL_AMBIENT, materials[matGround].ambient);
        glMaterialfv(GL_FRONT, GL_DIFFUSE, materials[matGround].diffuus);
        glMaterialfv(GL_FRONT, GL_SPECULAR, materials[matGround].specular);
        glMaterialf(GL_FRONT, GL_SHININESS, materials[matGround].shininess);
        
        for(i=0; i<q; i++){
            for(j=0; j<q; j++){
            glBegin(GL_QUADS);
                glVertex3f(i*a,0,j*a);
                glVertex3f(i*a+a,0,j*a);
                glVertex3f(i*a+a,0,j*a+a);
                glVertex3f(i*a,0,j*a+a);
           glEnd();
           }
        }
    }

    glPopMatrix();
}

void drawDrone()
{
    /*
     * Draw the drone
     */
    /* We need this variabels also for placing the drone on the floor */
    const int lengtePoot = 15;
    const float onder = sin(45.0/180.0*M_PI)*lengtePoot;
    const int posRot[4][3] = {{50, 5 , 25}, {25, 5, 50}, {75, 5, 50}, {50, 5, 75}};
    const int posLand[4][4] = {{50, 0, 40, 90}, {60, 0, 50, 0}, {40, 0, 50, 180}, {50, 0, 60, -90}};
    int i;

    glPushMatrix();

    /* Move drone up on the floor */
    glTranslatef(0, onder, 0);

    /* Select material for body */
    glMaterialfv(GL_FRONT, GL_AMBIENT, materials[matFrame].ambient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, materials[matFrame].diffuus);
    glMaterialfv(GL_FRONT, GL_SPECULAR, materials[matFrame].specular);
    glMaterialf(GL_FRONT, GL_SHININESS, materials[matFrame].shininess + extraShininess);

    /* Draw body */
	glPushMatrix();
		glTranslatef(50, 0, 50);
		frame(mode, q);
	glPopMatrix();


    /* Select material for propellors */
    glMaterialfv(GL_FRONT, GL_AMBIENT, materials[matProp].ambient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, materials[matProp].diffuus);
    glMaterialfv(GL_FRONT, GL_SPECULAR, materials[matProp].specular);
    glMaterialf(GL_FRONT, GL_SHININESS, materials[matProp].shininess + extraShininess);

    /* draw the propellors */
    for(i=0; i<4; i++){
        glPushMatrix();
            glTranslatef(posRot[i][0], posRot[i][1], posRot[i][2]);
            propellor(hoekProp, mode, q, showPoints);
        glPopMatrix();
    }
   
    /* Select material for landing */ 
    glMaterialfv(GL_FRONT, GL_AMBIENT, materials[matPoot].ambient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, materials[matPoot].diffuus);
    glMaterialfv(GL_FRONT, GL_SPECULAR, materials[matPoot].specular);
    glMaterialf(GL_FRONT, GL_SHININESS, materials[matPoot].shininess + extraShininess);

    /*Draw the four landing gears */
    for(i=0; i<4; i++){
        glPushMatrix();
            glTranslatef(posLand[i][0], posLand[i][1], posLand[i][2]);
            glRotatef(posLand[i][3], 0, 1, 0);
            landingsPoot(lengtePoot, onder/2, hoekLand, mode);
        glPopMatrix();
    }
    
    
    /* Select material for body */
    glMaterialfv(GL_FRONT, GL_AMBIENT, materials[matLich].ambient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, materials[matLich].diffuus);
    glMaterialfv(GL_FRONT, GL_SPECULAR, materials[matLich].specular);
    glMaterialf(GL_FRONT, GL_SHININESS, materials[matLich].shininess + extraShininess);
   
    /* Enable blending and draw body */
    if(blend){
        glEnable(GL_BLEND);
    }

    glPushMatrix();
        glTranslatef(50, 0, 50);
        lichaam(mode, showPoints);
    glPopMatrix();
    
    if(blend){
        glDisable(GL_BLEND);
    }
 
    glPopMatrix();
}


void anim(int delta)
{
    /*
     * Callback function for the animation
     * This calculates the height, rotation of the drone,
     * the rotation of the propellors and the rotation of the landinggear
     */

    if(draai){
        hoekProp += draai * 8 * delta;
        if(hoekProp > 360.0){
            hoekProp -= 360.0;
        }
        else if(hoekProp < -360.0){
            hoekProp += 360.0;
        }
        
        if(vlieg == 1){
			if(hoogte < maxHoogte){
				/* Stijgen */
				hoogte += delta;
			}
			else if(hoogte > maxHoogte){
				/* Dalen */
				hoogte -= delta;
			}
			else{
				/* rondvliegen */
				hoekDrone += delta * draai;
				if(hoekDrone > 360.0){
					hoekDrone -= 360.0;
				}
				else if(hoekDrone < -360.0){
					hoekDrone += 360.0;
				}
			}
		}
		else if(vlieg == 2){
			/* We need to land */
			hoogte -= delta;
			if(hoogte == 0){
				/* geland */
				vlieg = 0;
			}
		}

        if((hoogte > MINH) && (hoekLand < MAXHOEKPOOT)){
            /* Landing gear up */
            hoekLand += delta * 2;
        }
        else if((hoogte < MINH) && (hoekLand > MINHOEKPOOT)){
            /* Landing gear down */
            hoekLand -= delta * 2;
        }
        /* Call this function again */
        glutTimerFunc(tijd,anim,1);
    }
    
    glutPostRedisplay();
}

int main (int argc, char* argv[])
{
	fprintf(stdout, "Drone opdracht OpenGL\nVan Tilt Bjorn\n2014-2015\n");
	int c;

    glutInit(&argc, argv);

    bugprint("%d %d\n", GL_LINE, GLU_LINE);

    while((c = getopt(argc, argv, "t:q:m:")) != -1){
        switch(c){
            case 't':
                tijd = atoi(optarg);
                break;
            case 'q':
                q = atoi(optarg);
                break;
            case 'm' :
                vis = atoi(optarg);
                break;
          default:
                fprintf(stderr, "Usage: %s [-t msec] [-q quality]\n", argv[0]);
                exit(1);
                break;
        }
    }

    init();

    glutMainLoop();    
    
    free(materials);
    return 0;
}
