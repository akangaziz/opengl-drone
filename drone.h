#ifndef DRONE_H
#define DRONE_H

#include <GL/glut.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>

#include "debug.h"
#include "materials.h"
#include "drawdrone.h"
#include "basicfig.h"
#include "InitJPG.h"

#define     TITLEPROG       "Drone Van Tilt Bjorn"

#define     MINHOEKPOOT     0
#define     MAXHOEKPOOT     30 
#define     MINH            50
#define     AANTALMENU      5

#define     AANTTEXT        2
#define     THELI           0
#define     TGRASS          1


/* Textures */
const char beeldnaam[AANTTEXT][80] = {"helipad.jpg", "grass.jpg"};

/* Menu names */
const char menuNamen[AANTALMENU][80] = {"Frame", "Lichaam", "Propellors", "LandingGear", "Ground"};

/* Functions */
void init();
void initLicht();
void initTexture();
void initMenu();
void toetsen(unsigned char key, int x, int y);
void myMenu(int id);
void raam(GLint newWidth, GLint newHeight);
void display();
void drawGround(int texture, GLdouble size, GLint q);
void drawDrone();

void anim(int delta);

#endif
