#ifndef BASICFIG_H
#define BASICFIG_H

#include <GL/glut.h>
#include <stdio.h>
#include <math.h>

#include "debug.h"

/* Functions */
void drawClosedCyl(GLenum mode, GLdouble rbasis, GLdouble rtop, GLdouble hooghte, GLint q);

#endif
